package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBuddyStrings(t *testing.T) {
	t.Run("Should be true for swpa and swap", func(t *testing.T) {
		got := bs("swpa", "swap")
		assert.True(t, got)
	})
	t.Run("Should be false for identical strings with more than one letter", func(t *testing.T) {
		got := bs("ab", "ab")
		assert.False(t, got)
	})
	t.Run("Should be for true for identical strings with only one letter", func(t *testing.T) {
		got := bs("aa", "aa")
		assert.True(t, got)
	})
	t.Run("Should be for false", func(t *testing.T) {
		got := bs("aaxy", "aacd")
		assert.False(t, got)
	})
	t.Run("Should be true for abxy and baxy", func(t *testing.T) {
		got := bs("abaa", "baaa")
		assert.True(t, got)
	})
	t.Run("Should be false for more than one swap in one go", func(t *testing.T) {
		got := bs("abcd", "badc")
		assert.False(t, got)
	})
}
