package main

func main() {

}

func bs(s, goal string) bool {
	// can't produce an identical string if one is longer than the other
	if len(s) != len(goal) {
		return false
	}
	swappables := make(map[rune]rune)
	charSet := make(map[rune]bool)
	hasDiff := false
	multiCharOccur := false
	swapSuccess := false
	for i, letter := range s {
		// identify multiple occurences of letters
		if charSet[letter] {
			multiCharOccur = true
		}
		charSet[letter] = true
		// found the only letter that makes a successful swap
		if hasDiff && rune(goal[i]) == swappables[letter] {
			swapSuccess = true
			continue
		}
		if s[i] != goal[i] {
			hasDiff = true
			swappables[rune(goal[i])] = letter
			if len(swappables) > 1 {
				return false
			}
		}
	}
	// identical strings fail
	// identical strings with multiple same letters are considered swappable
	return multiCharOccur || swapSuccess
}
